const bcrypt = require('bcrypt'),
    jwt = require('jsonwebtoken'),
    User = require('../models/user.model'),
    Contact = require('../models/contact.model'),
    userValidator = require('../validators/user.validator');



module.exports = {
    signup: async (req, res) => {
        try {
            const { body } = req;
            const { error } = userValidator.userCreate.validate(body);
            if (error)
                throw error;
            const newUser = new User(body);
            let user = await newUser.save();
            user = JSON.parse(JSON.stringify(user));
            delete user.password;
            console.log(user);
            return res.json(user);
        } catch (error) {
            return res.json({ error: error.message });
        }
    },

    login: async (req, res) => {
        const { email, password } = req.body;
        try {
            let user = await User.findOne({ email: email });
            if (!user) {
                return res.status(404).send({ success: false, message: "User not found" });
            }
            const match = await bcrypt.compare(password, user.password);
            if (!match) {
                return res.status(400).send({ success: false, message: "Password is invalid" });
            }
            user = JSON.parse(JSON.stringify(user));
            delete user.password;
            const token = jwt.sign(user, process.env.JWT_SECRET, { expiresIn: 3600 });
            return res.json({ success: true, user: user, token });
        } catch (error) {
            return res.status(500).send({ success: false, message: error.message });
        }
    },

    createContact: async (req, res) => {
        const { userId, userContact } = req.body;
        try {
            const contact = new Contact();
            contact.userId = userId;
            contact.userContact = userContact;
            await contact.save();
            return res.json({ success: true, message: 'User contact successfully added!' });
        } catch (error) {
            return res.json({ success: false, message: error.message });
        }
    },

    updateContact: async (req, res) => {
        const { userId, userContact } = req.body;
        try {
            if (userId && userContact) {
                await Contact.findOneAndUpdate({ userId: userId }, { contact: userContact }, { upsert: true });
                return res.json({ success: true, message: 'contact successfully updated!' });
            }
            else
                throw new Error('Authentication error');
        } catch (error) {
            return res.json({ success: false, message: error.message });

        }
    },

    getAllContacts: async (req, res) => {
        try {
            const users = await User.find();
            return res.json({ users });
        } catch (error) {
            return res.json({ success: false, message: error.message });

        }
    }

}
