const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const ContactSchema = new Schema({
    userId: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    contact: {
        type: String,
        default: ''
    }
});



module.exports = mongoose.model('Contact', ContactSchema);