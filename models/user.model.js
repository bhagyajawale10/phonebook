const mongoose = require('mongoose'),
    bcrypt = require('bcrypt'),
    Joi = require('joi'),
    Schema = mongoose.Schema;

const UserSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        default: ''
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

UserSchema.pre('save', function (next) {
    const that = this;
    if (this.isModified('password')) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                next(err);
            }
            else {
                bcrypt.hash(that.password, salt, function (err, hash) {
                    if (err)
                        next(err);
                    else {
                        that.password = hash;
                        next();
                    }
                })
            }
        })
    }
})

module.exports = mongoose.model('User', UserSchema);