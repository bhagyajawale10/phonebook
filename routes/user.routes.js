const userController = require('../controllers/user.controller');
validateToken = require('../utils').validateToken;

module.exports = function (app) {
    app.route('/signup').post(userController.signup);
    app.route('/login').post(userController.login);
    app.route('/user/contact')
        .post(validateToken,userController.createContact)
        .put(validateToken, userController.updateContact);
    app.route('/contacts').get(userController.getAllContacts);
}