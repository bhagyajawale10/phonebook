const Joi = require('@hapi/joi');

module.exports = {
    userCreate: Joi.object().keys({
        firstName: Joi.string().alphanum().min(3).max(30).required().error(new Error('first name is required and it should be of length of three to 30 characters')),
        lastName: Joi.string().alphanum().min(3).max(30).required().error(new Error('last name is required and it should be of length of three to 30 characters')),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required().error(new Error('Password is required and should have number, lowercase and uppercase text')),
        email: Joi.string().email({ minDomainSegments: 2 }).required().error(new Error('email is required with domain format'))
    })
}